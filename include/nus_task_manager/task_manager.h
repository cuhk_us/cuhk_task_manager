// Copyright 2017 Yuchao Hu

#ifndef NUS_TASK_MANAGER_TASK_MANAGER_H
#define NUS_TASK_MANAGER_TASK_MANAGER_H

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <sensor_msgs/Range.h>
#include <vector>
#include "nus_msgs/Communication.h"
#include "nus_msgs/PoseWithIdStamped.h"
#include "nus_msgs/StateStamped.h"
#include "nus_msgs/StateWithCovarianceStamped.h"
#include "nus_msgs/StateWithCovarianceMaskStamped.h"
#include "nus_msgs/StartTaskManager.h"
#include "std_msgs/Bool.h"
#include "nus_reference_generator/response.h"

namespace nus_task_manager
{
enum TaskState
{
  kUninit,
  kReady,
  kTakingOff,
  kHoverTurning,
  kNavigating,
  kWaiting,
  kLanding,
  kLanded
};

//! Assign, manage and control tasks.
/*!
 * Auto task switch based on preplanned target and current uav state;
 * assign sub task to path planning module;
 * send commands to mission control module to control task proceeding.
 */
class TaskManager
{
public:
  TaskManager();
  ~TaskManager();

private:
  void stateMeasurementCallback(const nus_msgs::StateWithCovarianceStamped::ConstPtr& msg_ptr);
  void stateReferenceCallback(const nus_msgs::StateWithCovarianceMaskStamped::ConstPtr& msg_ptr);
  void pathPlanTargetCallback(const geometry_msgs::PoseStamped::ConstPtr& msg_ptr);
  void pathPlanWaypointCallback(const nus_msgs::PoseWithIdStamped::ConstPtr& msg_ptr);
  void publishToPathPlanning();
  void PublishToReferenceGenerator();

  /*!
   * \brief Main loop of task manager, it is called periodically
   * \param [in] event Time event
   * \return Nothing
   */
  void mainLoop(const ros::TimerEvent& event);

  /*!
   * \brief Update task status
   * \return Nothing
   */
  void updateTask();

  /*!
   * \brief Callback of start task manager request from mission control
   * \param [in] service request to start task manager
   * \return True
   */
  bool startTaskManegerCallback(nus_msgs::StartTaskManager::Request& req,
                                nus_msgs::StartTaskManager::Response& res);
  /*!
   * \brief Callback of reach flag from reflexxes
   * \param [in] action respond message
   * \return True
   */
  void reachFlagCb(const nus_reference_generator::response::ConstPtr& msg);
  /*!
   * \brief Distance between two points
   * \param [in] x0 First point x
   * \param [in] y0 First point y
   * \param [in] x1 Second point x
   * \param [in] y1 Second point y
   * \return Distance between two points
   */
  template <typename T>
  double distance(T x0, T y0, T x1, T y1);

private:
  double distance_check_circle_radius_;
  double distance_to_end_point_;

  TaskState task_state_;

private:
  ros::NodeHandle node_;

  ros::Timer main_loop_timer_;

  ros::Subscriber state_measurement_sub_;
  ros::Subscriber state_reference_sub_;
  ros::Subscriber path_plan_target_sub_;
  ros::Subscriber path_plan_waypoint_sub_;
  ros::Subscriber reach_flag_sub_;

  ros::ServiceServer start_task_manager_server_;

  ros::Publisher current_target_pub_;
  ros::Publisher task_waypoint_pub_;

  nus_msgs::StateWithCovarianceStamped::ConstPtr state_measurement_ptr_;
  nus_msgs::StateWithCovarianceMaskStamped::ConstPtr state_reference_ptr_;
  geometry_msgs::PoseStamped::ConstPtr path_plan_local_target_ptr_;

  std::vector<nus_msgs::StateStamped> task_waypoints_;
  geometry_msgs::Pose initial_pose_;
  bool bypass_pathplanning_;
  int current_task_waypoint_index_;

  int target_xy_reached_counter_;

  bool keep_running_;
  bool reference_reached_;
  bool NavigateWithTraj;

  double start_time_;
  bool proceed_to_navigate_;
  int secs_to_navigation;
  
  double time_bf_navigation;
};

}  // namespace nus_task_manager

#endif  // NUS_TASK_MANAGER_TASK_MANAGER_H
