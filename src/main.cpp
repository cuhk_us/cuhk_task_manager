// Copyright 2017 Yuchao Hu
#include <ros/ros.h>
#include "nus_task_manager/task_manager.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "task_manager");

  nus_task_manager::TaskManager t;

  ros::spin();

  return 0;
}
