// Copyright 2017 Yuchao Hu

#include "nus_task_manager/task_manager.h"
#include <geometry_msgs/PoseStamped.h>
#include <nus_msgs/StateStamped.h>
#include <ros/package.h>
#include <tf/transform_datatypes.h>
#include <cmath>
#include <limits>

#ifndef PI
#define PI 3.14159265359
#endif

namespace nus_task_manager
{
TaskManager::TaskManager()
  : keep_running_(false)
  , state_measurement_ptr_(nullptr)
  , state_reference_ptr_(nullptr)
  , current_task_waypoint_index_(0)
  , target_xy_reached_counter_(0)
  , NavigateWithTraj(false)
  , start_time_(0)
  , proceed_to_navigate_(false)
  , secs_to_navigation(0)
{
  /* init ros */
  ros::NodeHandle private_nh("~");

  private_nh.param("NavigateWithTraj", NavigateWithTraj, false);

  private_nh.param("initial_position_x", initial_pose_.position.x, 0.0);
  private_nh.param("initial_position_y", initial_pose_.position.y, 0.0);
  private_nh.param("initial_position_z", initial_pose_.position.z, 0.0);
  private_nh.param("dist_check", distance_check_circle_radius_, 0.5);
  // ROS_INFO("dist_check_value: %f", distance_check_circle_radius_);
  // Timer for navigation mission
  private_nh.param("time_bf_navigation", time_bf_navigation, 20.0);

  main_loop_timer_ = node_.createTimer(ros::Duration(0.02), &TaskManager::mainLoop, this);
  state_measurement_sub_ = node_.subscribe("/state/measurement", 1, &TaskManager::stateMeasurementCallback, this);
  state_reference_sub_ = node_.subscribe("/state/reference", 1, &TaskManager::stateReferenceCallback, this);
  start_task_manager_server_ = node_.advertiseService("/nus_task_manager/start_task_manager", &TaskManager::startTaskManegerCallback, this);

  // final target to reference generator
  current_target_pub_ = node_.advertise<nus_msgs::StateStamped>("/nus_task_manager/current_target", 1);

  // distance_check_circle_radius_ = 0.5 * 0.5;
  distance_to_end_point_ = 1.0 * 1.0;

  task_state_ = kUninit;


  // Reading waypoints from the yaml file 
  XmlRpc::XmlRpcValue waypoints;
  private_nh.getParam("waypoints", waypoints);
  ROS_ASSERT(waypoints.size() > 0);
  // ROS_INFO_STREAM("waypoint size: " << waypoints.size());
  task_waypoints_.clear();
  task_waypoints_.reserve(waypoints.size());
  

  // store the waypoint one by one
  for (int i = 0; i < waypoints.size(); ++i)
  {
    XmlRpc::XmlRpcValue data_list(waypoints[i]);
    // Check whether the waypoints are in the correct format
    if(NavigateWithTraj)
      ROS_ASSERT(10 == data_list.size());
    else
      ROS_ASSERT(3 == data_list.size());
    // nus_msgs for storing both position and velocity
    nus_msgs::StateStamped Target;
    // store the target pose
    Target.pose.position.x = data_list[0];
    Target.pose.position.y = data_list[1];
    Target.pose.position.z = data_list[2];

    // initialize the orientation
    Target.pose.orientation.x = 0.0;
    Target.pose.orientation.y = 0.0;
    Target.pose.orientation.z = 0.0;
    Target.pose.orientation.w = 1.0;
    
    

    if(NavigateWithTraj){
      // store target velocity
    Target.velocity.linear.x = data_list[3];
    Target.velocity.linear.y = data_list[4];
    Target.velocity.linear.z = data_list[5];
    // store acceleration
    Target.acceleration.linear.x = data_list[6];
    Target.acceleration.linear.y = data_list[7];
    Target.acceleration.linear.z = data_list[8];
    // store the orietation 
    tf::Quaternion q;
    q.setRPY(0, 0, data_list[9]);
    tf::quaternionTFToMsg(q, Target.pose.orientation);
    }

    task_waypoints_.push_back(Target);
  }

  ROS_ASSERT(initial_pose_.position.x == task_waypoints_.at(0).pose.position.x);
  ROS_ASSERT(initial_pose_.position.y == task_waypoints_.at(0).pose.position.y);
  ROS_ASSERT(initial_pose_.position.z == task_waypoints_.at(0).pose.position.z);

  // store the heading for each way point, note that heading for the first waypoint is not used
  if(!NavigateWithTraj){
    for (int i = 1; i < task_waypoints_.size() - 1; ++i)
    {
      double heading = atan2((task_waypoints_.at(i + 1).pose.position.y - task_waypoints_.at(i).pose.position.y),
	           (task_waypoints_.at(i + 1).pose.position.x - task_waypoints_.at(i).pose.position.x));
      ROS_INFO("heanding: %f ", heading);
      tf::Quaternion q;
      q.setRPY(0, 0, heading);
      tf::quaternionTFToMsg(q, task_waypoints_.at(i + 1).pose.orientation);
    }
  }

  initial_pose_.orientation.x = 0.0;
  initial_pose_.orientation.y = 0.0;
  initial_pose_.orientation.z = 0.0;
  initial_pose_.orientation.w = 1.0;
}

TaskManager::~TaskManager()
{
}

void TaskManager::stateMeasurementCallback(const nus_msgs::StateWithCovarianceStamped::ConstPtr& msg_ptr)
{
  state_measurement_ptr_ = msg_ptr;
}

void TaskManager::stateReferenceCallback(const nus_msgs::StateWithCovarianceMaskStamped::ConstPtr& msg_ptr)
{
  state_reference_ptr_ = msg_ptr;
}

void TaskManager::mainLoop(const ros::TimerEvent& event)
{
  if (!keep_running_)
    return;
  
  if(keep_running_ && !proceed_to_navigate_ && 
     secs_to_navigation != int(time_bf_navigation - (ros::Time::now().toSec() - start_time_)))
  { 
    secs_to_navigation = int(time_bf_navigation - (ros::Time::now().toSec() - start_time_));
    ROS_INFO("%d sec to navigation...", secs_to_navigation);
  }
  
  if(keep_running_ && (ros::Time::now().toSec() - start_time_) > time_bf_navigation && !proceed_to_navigate_)
  {
    proceed_to_navigate_ = true;
  }
  // Task updating should be at high frequency if the waypoints are very close to each other
  updateTask();
  PublishToReferenceGenerator();
}

void TaskManager::updateTask()
{
  if (!state_measurement_ptr_)
    return;
  //ROS_INFO_STREAM("current_task_waypoint_index_: " << current_task_waypoint_index_<<"current_task_waypoint_index_.position.x: " <<task_waypoints_.at(current_task_waypoint_index_).pose.position.z);
  const float dist_x =
      task_waypoints_.at(current_task_waypoint_index_).pose.position.x - state_measurement_ptr_->pose.pose.position.x;
  const float dist_y =
      task_waypoints_.at(current_task_waypoint_index_).pose.position.y - state_measurement_ptr_->pose.pose.position.y;
  const float dist_to_current_target_sqr = dist_x * dist_x + dist_y * dist_y;

  if (dist_to_current_target_sqr < distance_check_circle_radius_ * distance_check_circle_radius_)
  {
    target_xy_reached_counter_++;
  }
  else
  {
    target_xy_reached_counter_ = 0;
  }
  bool is_target_xy_reached;
  if (target_xy_reached_counter_ > 3)  /*FIXME*/ // wait for 3 readings
  {
    is_target_xy_reached = true;
  }
  else
  {
    is_target_xy_reached = false;
  }

  switch (task_state_)
  {
    case kUninit:
    {
      task_state_ = kReady;
      ROS_INFO_STREAM("[Task Manager] uninit -> ready");
      break;
    }
    case kReady:
    {
      if (current_task_waypoint_index_ + 1 < task_waypoints_.size())
      {
        if (0 == task_waypoints_.at(current_task_waypoint_index_).pose.position.z &&
            0 < task_waypoints_.at(current_task_waypoint_index_ + 1).pose.position.z)
        {
          ++current_task_waypoint_index_;
          task_state_ = kTakingOff;
          ROS_INFO("[Task Manager] ready -> taking off: [%f, %f, %f, %f, %f, %f]", task_waypoints_.at(current_task_waypoint_index_).pose.position.x, task_waypoints_.at(current_task_waypoint_index_).pose.position.y, task_waypoints_.at(current_task_waypoint_index_).pose.position.z, task_waypoints_.at(current_task_waypoint_index_).velocity.linear.x, task_waypoints_.at(current_task_waypoint_index_).velocity.linear.y, task_waypoints_.at(current_task_waypoint_index_).velocity.linear.z);
        }
      }
      if (kTakingOff != task_state_)
      {
        ROS_WARN_STREAM("[Task Manager] ready -> taking off: failed");
      }
      break;
    }
    case kTakingOff:
    {
      if (fabs(state_measurement_ptr_->pose.pose.position.z -
               task_waypoints_.at(current_task_waypoint_index_).pose.position.z) < 0.2 &&
          is_target_xy_reached && proceed_to_navigate_)
      {
        task_state_ = kNavigating;
        ROS_INFO("[Task Manager] taking off -> navigating");
      }
      break;
    }
    case kNavigating:
    {
      if (current_task_waypoint_index_ + 1 < task_waypoints_.size() 
      && (NavigateWithTraj||is_target_xy_reached && proceed_to_navigate_))
      {
            if (0 < task_waypoints_.at(current_task_waypoint_index_).pose.position.z &&
              0 == task_waypoints_.at(current_task_waypoint_index_ + 1).pose.position.z)
          {
            if(!NavigateWithTraj)
            {
              ROS_INFO("[Task Manager] reached points: %d [%f, %f, %f] ",
            current_task_waypoint_index_, 
            task_waypoints_.at(current_task_waypoint_index_).pose.position.x, 
            task_waypoints_.at(current_task_waypoint_index_).pose.position.y,
            task_waypoints_.at(current_task_waypoint_index_).pose.position.z);
            }
            ++current_task_waypoint_index_;
            task_state_ = kLanding;
            ROS_INFO("[Task Manager] navigating -> landing: [%f, %f, %f]", 
            task_waypoints_.at(current_task_waypoint_index_).pose.position.x, 
            task_waypoints_.at(current_task_waypoint_index_).pose.position.y,
            task_waypoints_.at(current_task_waypoint_index_).pose.position.z);
          }
          else 
          {
            // navigation stage
            if(!NavigateWithTraj)
            {
              ROS_INFO("[Task Manager] reached points: %d [%f, %f, %f] " , 
            current_task_waypoint_index_, 
            task_waypoints_.at(current_task_waypoint_index_).pose.position.x, 
            task_waypoints_.at(current_task_waypoint_index_).pose.position.y,
            task_waypoints_.at(current_task_waypoint_index_).pose.position.z);
            }
            
            ++current_task_waypoint_index_;
            task_state_ = kNavigating;
          }
      }
      break;
    }
    case kLanding:
    {
      if (state_measurement_ptr_->pose.pose.position.z < 0.2)
      {
        task_state_ = kLanded;
      }
      break;
    }
    case kHoverTurning:
    case kLanded:
    {
      break;
    }
    default:
    {
      ROS_WARN_STREAM("[Task manager] Unknown task state!");
      break;
    }
  }
}

void TaskManager::PublishToReferenceGenerator()
{

  // reference
  nus_msgs::StateStamped msg_to_reference_generator;
  msg_to_reference_generator.header.stamp = ros::Time::now();
  msg_to_reference_generator.header.frame_id = "/map";

  msg_to_reference_generator = task_waypoints_.at(current_task_waypoint_index_);
//  ROS_INFO("[task manager] Current waypoint: x %f, y %f, z %f",msg_to_reference_generator.pose.position.x);
  current_target_pub_.publish(msg_to_reference_generator);
}

bool TaskManager::startTaskManegerCallback(nus_msgs::StartTaskManager::Request& req,
                                           nus_msgs::StartTaskManager::Response& res)
{
  if (req.start == true){
    keep_running_ = true;
    start_time_ = ros::Time::now().toSec();
    ROS_INFO("[Task manager] Task manager started!");
  } else {
    keep_running_ = false;
    ROS_INFO("[Task manager] Task manager stopped!");
  }
  res.started = (keep_running_ == true);

  return true;
}

template <typename T>
double TaskManager::distance(T x0, T y0, T x1, T y1)
{
  double dx = x1 - x0;
  double dy = y1 - y0;
  return sqrt(dx * dx + dy * dy);
}

}  // namespace nus_task_manager
