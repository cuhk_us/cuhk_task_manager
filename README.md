# Overview
This is the main control task of AUTO_NAV software. It reads waypoints in config/xxxx.yaml file in NWU and navigate the waypoints.

## What does this software do?
Manage, assign and switch tasks for UAV.

## Specifications
Simple design.

## Limitations
Only support one UAV.

## Waypoint list reading
* x,y,z in north west up frame in ROS

# ROS API

## nus\_task\_manager

### Subscribed Topics
- */state/measurement (nus_msgs::StateWithCovarianceStamped)*
- */state/reference (nus_msgs::StateWithCovarianceStamped)*
- */reference_generator/task_manager_start_cmd (std_msgs::Bool)*
Trigger task manager running.
- */pathplanning/target_pose (geometry_msgs::PoseStamped)*
Valid if param pypass_pathplanning is true. Local target from pathplanning.
- */pathplanning/target_pose_way_point (nus_msgs::PoseWithIdStamped)*
Valid if param pypass_pathplanning is true. Corrected waypoint from path planning.


### Published Topics
- */task_manager/local_waypoint (nus_msgs::PoseWithIdStamped)*
Valid if param pypass_pathplanning is true. Waypoint to path planning.
- */task_manager/current_target (geometry_msgs::PoseStamped)*
Final target to reference generator.

### Parameters
- *initial_position_[x|y|z]* (double, default: 0.0)
Initial position of task manager.
- *pypass_pathplanning* (bool, default: false)
If true, pypass pathplanning, send target to reference generator directly.
